package com.example.ws;

import com.example.crud.dto.LoginDTO;
import com.example.crud.dto.ResponseDTO;
import com.example.crud.dto.UserDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface IUserApi {

    @GET("user/test")
    Call<ResponseDTO> test();

    @GET("user/list")
    Call<ResponseDTO> listAll();

    @POST("user/login")
    Call<ResponseDTO> login(@Body LoginDTO dto);

    @POST("user")
    Call<ResponseDTO> register(@Body UserDTO dto);
}
