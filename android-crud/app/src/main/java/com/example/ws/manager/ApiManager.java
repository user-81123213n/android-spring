package com.example.ws.manager;

import com.example.crud.dto.LoginDTO;
import com.example.crud.dto.ResponseDTO;
import com.example.crud.dto.UserDTO;
import com.example.ws.IUserApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiManager {

    private static IUserApi userApi;

    private static final String BASE_URL = "http://192.168.1.68:8080/api/";

    private static class SingletonHelper {

        private static final ApiManager INSTANCE = new ApiManager();

        public static ApiManager getInstance() {
            return SingletonHelper.INSTANCE;
        }
    }

    private ApiManager() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        userApi = retrofit.create(IUserApi.class);
    }

    public static ApiManager getInstance() {
        return SingletonHelper.getInstance();
    }

    public void test(Callback<ResponseDTO> callback) {
        Call<ResponseDTO> regionsCall = userApi.test();
        regionsCall.enqueue(callback);
    }

    public void userList(Callback<ResponseDTO> callback) {
        Call<ResponseDTO> regionsCall = userApi.listAll();
        regionsCall.enqueue(callback);
    }

    public void userLogin(Callback<ResponseDTO> callback, LoginDTO dto) {
        Call<ResponseDTO> regionsCall = userApi.login(dto);
        regionsCall.enqueue(callback);
    }

    public void userRegister(Callback<ResponseDTO> callback, UserDTO dto) {
        Call<ResponseDTO> regionsCall = userApi.register(dto);
        regionsCall.enqueue(callback);
    }
}
