package com.example.crud.ui;

import static android.content.Context.MODE_PRIVATE;
import static com.example.crud.constant.ICrud.HAS_LOGIN;
import static com.example.crud.constant.ICrud.SHARED_PREF_NAME;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.crud.R;
import com.example.crud.dto.LoginDTO;
import com.example.crud.dto.ResponseDTO;
import com.example.crud.dto.UserDTO;
import com.example.crud.utils.GsonHelper;
import com.example.crud.utils.SharedPrefManager;
import com.example.ws.manager.ApiManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Button loginBtn = null;
    private Button registerBtn = null;
    private EditText emailTxt = null;
    private EditText passwordTxt = null;

    @Override
    public void onStart() {
        super.onStart();
        Boolean isLogin = (Boolean) SharedPrefManager.getObject(getSharedPreferences(SHARED_PREF_NAME,
                MODE_PRIVATE),
                HAS_LOGIN, Boolean.TYPE);
        if (isLogin != null && isLogin) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setViews();

        registerBtn.setOnClickListener(view -> {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
        });

        loginBtn.setOnClickListener(view -> {
            LoginDTO loginDTO = LoginDTO.builder()
                    .email(emailTxt.getText().toString())
                    .password(passwordTxt.getText().toString())
                    .build();
            ApiManager.getInstance().userLogin(new Callback<ResponseDTO>() {
                @Override
                public void onResponse(Call<ResponseDTO> call, Response<ResponseDTO> response) {
                    ResponseDTO responseDTO = response.body();
                    UserDTO userDTO = GsonHelper.gson().fromJson(responseDTO.getBody().toString(), UserDTO.class);

                    SharedPrefManager.setSession(getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE),
                            userDTO);

                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                }

                @Override
                public void onFailure(Call<ResponseDTO> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, getResources().getText(R.string.failed_login),
                            Toast.LENGTH_SHORT);
                }
            }, loginDTO);
        });
    }

    private void setViews() {
        loginBtn = findViewById(R.id.lg_login_btn);
        registerBtn = findViewById(R.id.lg_register_btn);
        emailTxt = findViewById(R.id.lg_email_txt);
        passwordTxt = findViewById(R.id.lg_password_txt);
    }
}
