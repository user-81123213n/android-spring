package com.example.crud.utils;

import static com.example.crud.constant.ICrud.HAS_LOGIN;
import static com.example.crud.constant.ICrud.USER_SHARED_PREF_KEY;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.example.crud.dto.UserDTO;

public final class SharedPrefManager {

    private SharedPrefManager() {
        throw new UnsupportedOperationException();
    }


    public static void putValue(SharedPreferences sharedPreferences, String key, Object value) {
        Editor editor = sharedPreferences.edit();
        editor.putString(key, GsonHelper.gson().toJson(value));
        editor.commit();
    }

    private static void removeValue(SharedPreferences sharedPreferences, String key) {
        Editor editor = sharedPreferences.edit();
        editor.putString(key, null);
        editor.commit();
    }

    public static String getValue(SharedPreferences sharedPreferences, String key) {
        return sharedPreferences.getString(key, "");
    }

    public static Object getObject(SharedPreferences sharedPreferences, String key, Class cls) {
        String json = sharedPreferences.getString(key, "");
        if (json == null || json.equals("")) {
            return null;
        }
        return GsonHelper.gson().fromJson(json, cls);
    }

    public static void setSession(SharedPreferences sharedPreferences, UserDTO userDTO) {
        putValue(sharedPreferences, USER_SHARED_PREF_KEY, userDTO);
        putValue(sharedPreferences, HAS_LOGIN, Boolean.TRUE);
    }

    public static void invalidSession(SharedPreferences sharedPreferences) {
        removeValue(sharedPreferences, USER_SHARED_PREF_KEY);
        removeValue(sharedPreferences, HAS_LOGIN);
    }
}
