package com.example.crud.ui;

import static com.example.crud.constant.ICrud.SHARED_PREF_NAME;

import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import com.example.crud.R;
import com.example.crud.ui.fragment.AboutFragment;
import com.example.crud.ui.fragment.HomeFragment;
import com.example.crud.ui.fragment.UsersFragment;
import com.example.crud.utils.SharedPrefManager;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends FragmentActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ActionBarDrawerToggle drawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setViews();

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        navigationView.bringToFront();
        navigationView.setCheckedItem(R.id.menu_profile);
        navigationView.setNavigationItemSelectedListener(this);

        HomeFragment homeFragment = new HomeFragment();
        displaySelectedFragment(homeFragment);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setViews() {
        drawerLayout = findViewById(R.id.my_drawer);
        navigationView = findViewById(R.id.my_nav_viewer);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        menuItem.setChecked(true);
        drawerLayout.closeDrawers();
        switch (menuItem.getItemId()) {
            case R.id.menu_profile: {

                break;
            }
            case R.id.menu_list_users: {
                Fragment usersFragment = new UsersFragment();
                displaySelectedFragment(usersFragment);
                break;
            }
            case R.id.menu_about: {
                Fragment aboutFragment = new AboutFragment();
                displaySelectedFragment(aboutFragment);
                break;
            }
            case R.id.menu_logout: {
                doLogout();
                break;
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void displaySelectedFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }

    private void doLogout() {
        buildAlertDialogForLogout();
    }

    private void buildAlertDialogForLogout() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(getResources().getString(R.string.exit_title));
        alertDialog.setMessage(getResources().getString(R.string.exit_msg));
        alertDialog.setIcon(R.drawable.info);
        alertDialog.setButton(Dialog.BUTTON_POSITIVE, getResources().getString(R.string.yes),
                (OnClickListener) (dialogInterface, i) -> SharedPrefManager
                        .invalidSession(getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE)));
        alertDialog.setButton(Dialog.BUTTON_NEGATIVE, getResources().getString(R.string.no),
                (OnClickListener) (dialogInterface, i) -> alertDialog.hide());
        alertDialog.show();
    }
}
