package com.example.crud.utils;

import com.google.gson.Gson;

public final class GsonHelper {

    private GsonHelper() {
        throw new UnsupportedOperationException();
    }

    private static class SingletonHelper {

        private static final Gson INSTANCE = new Gson();

        public static Gson getGson() {
            return INSTANCE;
        }
    }

    public static Gson gson() {
        return GsonHelper.SingletonHelper.getGson();
    }
}
