package com.example.crud.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.crud.R;
import com.example.crud.dto.ResponseDTO;
import com.example.crud.dto.UserDTO;
import com.example.crud.utils.GsonHelper;
import com.example.ws.manager.ApiManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private TextView signTxt;
    private EditText emailTxt;
    private EditText usernameTxt;
    private EditText passwordTxt;
    private EditText firstNameTxt;
    private EditText lastNameTxt;
    private EditText ageTxt;
    private Button registerBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setViews();

        signTxt.setOnClickListener(view -> {
            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        });

        registerBtn.setOnClickListener(view -> {
            UserDTO userDTO = UserDTO.builder()
                    .email(emailTxt.getText().toString().trim())
                    .username(usernameTxt.getText().toString().trim())
                    .firstName(firstNameTxt.getText().toString().trim())
                    .lastName(lastNameTxt.getText().toString().trim())
                    .password(passwordTxt.getText().toString().trim())
                    .age(Integer.parseInt(ageTxt.getText().toString()))
                    .build();

            ApiManager.getInstance().userRegister(new Callback<ResponseDTO>() {
                @Override
                public void onResponse(Call<ResponseDTO> call, Response<ResponseDTO> response) {
                    ResponseDTO responseDTO = response.body();
                    Boolean isSuccess = GsonHelper.gson().fromJson(responseDTO.getBody().toString(), Boolean.class);

                    if (isSuccess) {
                        Toast.makeText(RegisterActivity.this, getResources().getText(R.string.success_msg),
                                Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                    } else {
                        Toast.makeText(RegisterActivity.this, getResources().getText(R.string.try_again),
                                Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDTO> call, Throwable t) {
                    Log.d(getResources().getString(R.string.trace), t.getMessage());
                }
            }, userDTO);
        });
    }

    private void setViews() {
        emailTxt = findViewById(R.id.reg_email_txt);
        usernameTxt = findViewById(R.id.reg_username_txt);
        passwordTxt = findViewById(R.id.reg_password_txt);
        firstNameTxt = findViewById(R.id.reg_first_name_txt);
        lastNameTxt = findViewById(R.id.reg_last_name_txt);
        ageTxt = findViewById(R.id.reg_age_txt);
        registerBtn = findViewById(R.id.reg_register_btn);
        signTxt = findViewById(R.id.reg_sign_in_txt);
    }
}
