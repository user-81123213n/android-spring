package com.example.server.rest;

import com.example.server.dto.LoginDTO;
import com.example.server.dto.UserDTO;
import com.example.server.service.UserService;
import com.example.server.utils.ResponseDTO;
import java.util.Arrays;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserRestController {

    @Autowired
    private UserService userService;

    @GetMapping("/test")
    public ResponseEntity<ResponseDTO> test() {
        ResponseDTO responseDTO = ResponseDTO.builder()
                .status(HttpStatus.OK.value())
                .body(Arrays.asList("hello world!")).build();

        return ResponseEntity.ok().body(responseDTO);
    }

    @GetMapping("/list")
    public ResponseEntity<ResponseDTO> findAll() {
        ResponseDTO responseDTO = ResponseDTO.builder()
                .status(HttpStatus.OK.value())
                .body(userService.findAll())
                .build();

        return ResponseEntity.ok().body(responseDTO);
    }

    @PostMapping("/login")
    public ResponseEntity<ResponseDTO> login(@Valid @RequestBody LoginDTO dto) {
        ResponseDTO responseDTO = ResponseDTO.builder()
                .status(HttpStatus.CREATED.value())
                .body(userService.login(dto).orElse(null))
                .build();

        return ResponseEntity.ok().body(responseDTO);
    }

    @PostMapping
    public ResponseEntity<ResponseDTO> register(@Valid @RequestBody UserDTO dto) {
        ResponseDTO responseDTO = ResponseDTO.builder()
                .status(HttpStatus.CREATED.value())
                .body(userService.register(dto)).build();

        return ResponseEntity.ok().body(responseDTO);
    }
}
