package com.example.server.utils;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseDTO<T> {

    private int status;

    @Builder.Default
    private String message = "Success";

    private T body;
}
