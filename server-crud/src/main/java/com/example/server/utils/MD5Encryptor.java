package com.example.server.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import javax.xml.bind.DatatypeConverter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class MD5Encryptor {

    private static byte[] SALT;
    private static final String INSTANCE_MSG = "unable to create instance";
    private static final String PASSWORD_IS_NULL_OR_EMPTY_MSG = "please set valid password";

    static {
        try {
            SALT = getSalt();
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            log.error(e.getMessage(), e);
        }
    }

    private MD5Encryptor() {
        throw new UnsupportedOperationException(INSTANCE_MSG);
    }

    public static String encrypt(String plainText) throws NoSuchAlgorithmException, NoSuchProviderException {
        if (plainText == null || "".equals(plainText)) {
            throw new IllegalArgumentException(PASSWORD_IS_NULL_OR_EMPTY_MSG);
        }
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(plainText.getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest);
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException, NoSuchProviderException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
        byte[] bytes = new byte[16];
        sr.nextBytes(bytes);
        return bytes;
    }
}
