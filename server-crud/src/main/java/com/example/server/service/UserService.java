package com.example.server.service;

import com.example.server.dto.LoginDTO;
import com.example.server.dto.UserDTO;
import java.util.List;
import java.util.Optional;

public interface UserService {

    Boolean register(UserDTO user);

    Optional<UserDTO> login(LoginDTO user);

    List<UserDTO> findAll();
}
