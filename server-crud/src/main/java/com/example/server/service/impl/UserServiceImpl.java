package com.example.server.service.impl;

import com.example.server.dto.LoginDTO;
import com.example.server.dto.UserDTO;
import com.example.server.entity.User;
import com.example.server.repository.UserRepository;
import com.example.server.service.UserService;
import com.example.server.utils.MD5Encryptor;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Boolean register(UserDTO dto) {
        User user = getUserFromDto(dto);
        return userRepository.save(user) != null ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public Optional<UserDTO> login(LoginDTO dto) {
        try {
            return Optional.ofNullable(convertToDto(
                    userRepository.login(dto.getEmail(), MD5Encryptor.encrypt(dto.getPassword()).toLowerCase())));
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            log.error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    @Override
    public List<UserDTO> findAll() {
        List<User> users = userRepository.findAll();
        List<UserDTO> dtos = new ArrayList<>();
        users.forEach(user -> dtos.add(convertToDto(user)));
        return dtos;
    }

    private User getUserFromDto(UserDTO dto) {
        User user = null;
        if (dto != null) {
            user = new User();
            if (dto.getEmail() != null) {
                user.setEmail(dto.getEmail());
            }
            if (dto.getFirstName() != null) {
                user.setFirstName(dto.getFirstName());
            }
            if (dto.getLastName() != null) {
                user.setLastName(dto.getLastName());
            }
            if (dto.getUsername() != null) {
                user.setUsername(dto.getUsername());
            }
            if (dto.getPassword() != null) {
                try {
                    user.setPassword(MD5Encryptor.encrypt(dto.getPassword()));
                } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
                    log.error(e.getMessage(), e);
                }
            }
            if (dto.getAge() != null) {
                user.setAge(dto.getAge());
            }
        }
        return user;
    }

    private UserDTO convertToDto(User user) {
        UserDTO dto = null;
        if (user != null) {
            dto = new UserDTO();
            if (user.getId() != null) {
                dto.setId(user.getId());
            }
            if (user.getEmail() != null) {
                dto.setEmail(user.getEmail());
            }
            if (user.getFirstName() != null) {
                dto.setFirstName(user.getFirstName());
            }
            if (user.getLastName() != null) {
                dto.setLastName(user.getLastName());
            }
            if (user.getUsername() != null) {
                dto.setUsername(user.getUsername());
            }
            if (user.getAge() != null) {
                dto.setAge(user.getAge());
            }
        }
        return dto;
    }
}
