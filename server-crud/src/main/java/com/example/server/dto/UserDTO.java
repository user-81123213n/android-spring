package com.example.server.dto;

import java.io.Serializable;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDTO implements Serializable {

    private static final long serialVersionUID = 7748876919048572885L;

    private Long id;

    @NotNull(message = "{first.name.not.empty}")
    @NotEmpty(message = "{first.name.not.empty}")
    private String firstName;

    @NotNull(message = "{last.name.not.empty}")
    @NotEmpty(message = "{last.name.not.empty}")
    private String lastName;

    @NotEmpty(message = "{email.not.empty}")
    @NotNull(message = "{email.not.empty}")
    @Email(message = "{email.not.valid}")
    private String email;

    @NotNull(message = "{username.not.empty}")
    @NotEmpty(message = "{username.not.empty")
    private String username;

    @NotNull(message = "{password.not.empty}")
    private String password;

    @NotNull(message = "{age.not.empty}")
    @Min(20)
    private Integer age;
}
