package com.example.server.dto;

import java.io.Serializable;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LoginDTO implements Serializable {

    private static final long serialVersionUID = 6250841846299199752L;

    @Email(message = "{email.not.valid}")
    @NotNull(message = "{email.not.empty}")
    @NotEmpty(message = "{email.not.empty}")
    private String email;

    @NotNull
    @NotEmpty(message = "{password.not.empty}")
    @Min(value = 9)
    private String password;

}
