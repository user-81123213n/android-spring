DROP TABLE IF EXISTS users;
CREATE TABLE users(
  id INT AUTO_INCREMENT primary key ,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  username VARCHAR(250) NOT NULL,
  password VARCHAR(250) NOT NULL,
  email VARCHAR(250) NOT NULL,
  age INT
  );
